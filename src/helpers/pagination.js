const pagination = {
  setPagination
};

function setPagination(data, pagination){
  pagination['totalPages'] = data.totalPages;
  pagination['currentPages'] = data.number;
  pagination['totalElements'] = data.totalElements;
  pagination['first'] = data.first;
  pagination['last'] = data.last;

  return pagination;
}

export default pagination;
