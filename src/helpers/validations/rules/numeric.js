const numeric = options => text => {
  const errorMessage = () => () => {
    const pattern = /^\d+$/;
    if (text) {
      if (pattern.test(text)) return false;
    }
    if (options) {
      if (options.errorMessage) {
        return options.errorMessage;
      }
    }
    return 'This field may only contain numeric characters';
  };

  return errorMessage();
};

export default numeric;
