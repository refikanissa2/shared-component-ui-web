const alphaNum = options => text => {
  const errorMessage = () => () => {
    const pattern = /^[a-zA-Z0-9]+$/;
    if (text) {
      if (pattern.test(text)) return false;
    }
    if (options) {
      if (options.errorMessage) {
        return options.errorMessage;
      }
    }
    return 'This field may contain alphabetic characters or numbers';
  };

  return errorMessage();
};

export default alphaNum;
