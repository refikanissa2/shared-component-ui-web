const email = options => text => {
  const errorMessage = () => () => {
    const pattern = /(.+)@(.+){2,}\.(.+){2,}/;
    if (text) {
      if (pattern.test(text)) return false;
    }
    if (options) {
      if (options.errorMessage) {
        return options.errorMessage;
      }
    }
    return 'This field must be a valid email';
  };

  return errorMessage();
};

export default email;
