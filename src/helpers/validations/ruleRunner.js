export const ruleRunner = (field, name, validations) => state => {
  for (let i = 0; i < validations.length; i += 1) {
    const errorMessageFunc = validations[i](state[field], state);
    if (errorMessageFunc) {
      if (errorMessageFunc(name)) {
        return { [field]: errorMessageFunc(name) };
      }
    }
  }
  return null;
};

export const run = (field, state, runners) => {
  const newState = Object.assign(state, {});
  if (!newState.touched) newState.touched = [];
  let newRunners = Object.keys(runners).map(key =>
    ruleRunner(key, runners[key].label, runners[key].validator)
  );

  newRunners = newRunners.reduce((memo, runner) => {
    if (newState.touched.indexOf(field) === -1) {
      newState.touched.push(field);
    }
    return Object.assign(memo, runner(newState));
  }, {});

  Object.keys(newRunners).forEach(key => {
    if (newState.touched.indexOf(key) === -1) {
      delete newRunners[key];
    }
  });
  return newRunners;
};

export const runAll = (state, runners) => {
  const newState = Object.assign(state, {});
  if (!newState.touched) newState.touched = [];
  let newRunners = [];
  Object.keys(runners).forEach(key => {
    if (newState.touched.indexOf(key) === -1) {
      newState.touched.push(key);
    }
    newRunners.push(
      ruleRunner(key, runners[key].label, runners[key].validator)
    );
  });
  newRunners = newRunners.reduce(
    (memo, runner) => Object.assign(memo, runner(newState)),
    {}
  );
  return newRunners;
};
