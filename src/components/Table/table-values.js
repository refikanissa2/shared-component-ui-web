import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = {
  values: {
    cursor: 'pointer'
  }
};

class TableValues extends Component {
  render() {
    const {onClick, style, isSelected} = this.props;
    var styleValue = styles.values;
    if(style) styleValue = style;
    return (
      <tr
        style={styleValue}
        onClick={() => onClick()}
        className={isSelected ? 'is-selected' : ''}
      >
        {this.props.children}
      </tr>
    );
  }
}

TableValues.propTypes = {
  onClick: PropTypes.func,
  style: PropTypes.object,
  isSelected: PropTypes.bool
};

TableValues.defaultProps = {
  onClick(){},
  isSelected: false
};

export default TableValues;
