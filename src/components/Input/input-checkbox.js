import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputCheckbox extends Component {

  constructor(){
    super();
    this.onChange = this.onChange.bind(this);
  }

  onChange(e){
    const value = e.target;
    this.props.onChange(value);
  }

  render() {
    const {
      value,
      name,
      disabled,
      width,
      id,
      style,
      text,
      checked
    } = this.props;
    let styles = style;
    if (width) styles = {...styles, width: width};
    return (
      <div style={{display: 'flex', alignItems: 'center'}}>
        <input
          value={value}
          checked={checked}
          name={name}
          type="checkbox"
          disabled={disabled}
          onChange={this.onChange}
          id={id}
          style={styles}
        />
        <label htmlFor={id} style={{paddingLeft: 5}}>{text}</label>
      </div>
    );
  }
}

InputCheckbox.propTypes = {
  value: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  width: PropTypes.number,
  style: PropTypes.object,
  text: PropTypes.string,
  checked: PropTypes.bool
};

InputCheckbox.defaultProps = {
  value: '',
  onChange() {},
  style: {},
  text: ''
};

export default InputCheckbox;
