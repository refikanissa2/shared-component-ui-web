import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputFile extends Component {

  constructor(props){
    super(props);
    this.state = {
      messages: this.props.language
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(e){
    const data = e.target;
    const {base64} = this.props;
    if(base64){
      let files = e.target.files;
      let allFiles = [];
      for(var i=0; i<files.length; i++){
        let file = files[i];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          let fileInfo = {
            name: file.name,
            type: file.type,
            size: file.size,
            base64: reader.result,
            file: file
          };

          allFiles.push(fileInfo);

          if(allFiles.length == files.length){
            if(this.props.multiple) this.props.onChange(data, allFiles);
            else this.props.onChange(data, allFiles[0]);
          }
        };
      }
    }else{
      let files = e.target.files[0];
      this.props.onChange(data, files);
    }

  }

  render() {
    const {size, id, name, filename, color, error, acceptTypeFile, disabled, multiple} = this.props;
    const {messages} = this.state;
    let className = 'button';
    if(size) className += ` is-${size}`;
    if(color) className += ` is-${color}`;
    let text = filename;
    let styleText = {paddingLeft: 10, paddingRight: 10};
    if(error){
      if(error != ''){
        text = error;
        styleText = {...styleText, color: 'red'};
      }
    }
    return (
      <div className="field" style={{display: 'flex', alignItems: 'center'}} >
        <label htmlFor={id} className={className} disabled={disabled} style={{color: 'white'}}>
          <i className="fa fa-folder-open" style={{marginRight: 5, color: 'white'}}></i> {messages.button.browse}
        </label>
        <input type="file"
          onChange={this.onChange}
          name={name}
          accept={acceptTypeFile}
          id={id}
          style={{display: 'none'}}
          disabled={disabled}
          multiple={multiple}
        />
        <span style={styleText}>{text}</span>
      </div>
    );
  }
}

InputFile.propTypes = {
  onChange: PropTypes.func,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'normal',
    'large'
  ]),
  color: PropTypes.oneOf([
    'white',
    'light',
    'black',
    'dark',
    'link',
    'primary',
    'info',
    'success',
    'warning',
    'danger'
  ]),
  acceptTypeFile: PropTypes.string,
  name: PropTypes.string,
  filename: PropTypes.string,
  id: PropTypes.any,
  error: PropTypes.string,
  language: PropTypes.object.isRequired,
  disabled: PropTypes.bool,
  multiple: PropTypes.bool,
  base64: PropTypes.bool,
};

InputFile.defaultProps = {
  onChange() {},
  size: 'small',
  color: 'primary',
  filename: 'No selected file',
  id: 'file-upload',
  multiple: false,
  base64: false
};

export default InputFile;
