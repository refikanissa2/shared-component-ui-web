import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputGeolocation extends Component {

  constructor(props){
    super(props);
    this.state={
      errorInput: false,
      valueError: '',
      errorLatLng: false,
      messages: this.props.language
    };
    this.validateInput = this.validateInput.bind(this);
  }

  validateInput(e){
    var inputVal = e.target.value;
    var letters = /^[A-Za-z]+$/;
    if(inputVal.match(letters)){
      this.setState({
        errorInput: true,
        valueError: inputVal.slice(-1)
      });
      return;
    }
    this.setState({
      errorInput: false,
      valueError: ''
    });
    const value = e.target;
    this.props.onChange(value);
  }

  validatePropsValue(){
    const {value} = this.props;
    const checkValue = value.match(/^[A-Za-z]+$/);

    if(checkValue) return false;

    return true;
  }

  render() {
    const {
      value,
      size,
      name,
      loading,
      disabled,
      placeholder,
      maxLength,
      width,
      index,
      error,
      hasAddon,
      children,
      autocomplete,
      hasIconLeft,
      hasIconRight,
      styleInput
    } = this.props;
    const {messages} = this.state;
    const validateValue = this.validatePropsValue(value) ? value : '';
    let className = 'input';
    let controlClassName = 'control';
    if(loading) controlClassName+=' is-loading';
    if(hasIconRight) controlClassName+= ' has-icons-right';
    if(hasIconLeft) controlClassName+=' has-icons-left';
    let hasAddonClass = hasAddon ? 'has-addons' : '';
    let style = width ? {width: width} : {};
    let styles = styleInput;
    if(width) styles = {width: width, ...styles};
    if (size) className+=` is-${size}`;
    if (error) className+=' is-danger';
    if (this.state.errorInput == true) className +=' is-danger';
    if (this.state.errorLatLng == true) className += ' is-danger';
    return (
      <div style={style}>
        <div className={`field ${hasAddonClass}`}>
          <div className={controlClassName}>
            <input
              value={validateValue}
              name={name}
              className={className}
              type="text"
              maxLength={maxLength}
              placeholder={placeholder}
              disabled={disabled}
              onChange={this.validateInput}
              style={styles}
              id={index}
              autoComplete={autocomplete}
            />
            {hasIconRight && hasIconRight}
            {hasIconLeft && hasIconLeft}
          </div>
          {hasAddon &&
            <div className="control input-addons">
              {children}
            </div>
          }
        </div>
        {!error && this.state.errorInput == true && <p className="help is-danger">{messages.text.errorSpecialChar +' '+ this.state.valueError}</p> }
        {!error && this.state.errorLatLng == true && <p className="help is-danger">Invalid data</p>}
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
}

InputGeolocation.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  maxLength : PropTypes.number,
  onChange: PropTypes.func.isRequired,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'large',
    'normal'
  ]),
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  width: PropTypes.number,
  hasAddon: PropTypes.bool,
  autocomplete: PropTypes.string,
  language: PropTypes.object.isRequired,
  hasIconLeft: PropTypes.element,
  hasIconRight: PropTypes.element,
  styleInput: PropTypes.object,
};

InputGeolocation.defaultProps = {
  value: '',
  maxLength: 50,
  size: 'small',
  onChange() {},
  autocomplete: 'off',
  styleInput: {}
};

export default InputGeolocation;
