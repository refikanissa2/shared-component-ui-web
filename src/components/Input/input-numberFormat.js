import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import ConvertFunc from '../../helpers/convert';

class InputNumberFormat extends Component {
  constructor(props){
    super(props);
    this.state = {
      messages: this.props.language
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(val){
    const {value} = val;
    const {name, id, decimalSeparator} = this.props;
    let newValue = value;
    if(decimalSeparator == ','){
      newValue = ConvertFunc.changeDecimalSeparator(value);
    }
    var data = {
      name: name,
      id: id,
      value: newValue
    };
    this.props.onChange(data);
  }

  render() {
    const {
      value,
      thousandSeparator, decimalSeparator, decimalScale,fixedDecimalScale,
      allowNegative,
      isAllowed,
      error,
      hasSymbol,
      currency,
      width,
      disabled,
      size,
      displayType,
      placeholder,
      hasText,
      hasAddon,
      loading,
      hasIconRight,
      hasIconLeft,
      children,
      prefix,
      suffix,
      textAlign,
      styleInput
    } = this.props;
    const {messages} = this.state;
    let className = 'input';
    if (size) className+=` is-${size}`;
    if (error) className+=' is-danger';
    let style = width ? {width: width} : {};
    let styles = styleInput;
    if(textAlign) styles.textAlign = textAlign;
    if(width) styles = {width: width, ...styles};
    let hasAddonClass = hasAddon ? 'has-addons' : '';
    let controlClassName = 'control is-expended';
    if(loading) controlClassName += ' is-loading';
    if(hasIconRight) controlClassName += ' has-icons-right';
    if(hasIconLeft) controlClassName += ' has-icons-left';
    return (
      <div style={style}>
        <div className={`field ${hasAddonClass}`} style={{marginBottom: '0rem'}}>
          {hasSymbol &&
            <div className="control ">
              <a className={`button is-static is-${className}`}>
                {currency}
              </a>
            </div>
          }
          <div className={controlClassName}>
            <NumberFormat
              value={value}
              thousandSeparator={thousandSeparator}
              decimalSeparator={decimalSeparator}
              decimalScale={decimalScale}
              fixedDecimalScale={fixedDecimalScale}
              allowNegative={allowNegative}
              onValueChange={this.onChange}
              isAllowed={isAllowed}
              className={className}
              style={styles}
              disabled={disabled}
              displayType={displayType}
              placeholder={placeholder}
              prefix={prefix}
              suffix={suffix}
            />
            {hasIconRight && hasIconRight}
            {hasIconLeft && hasIconLeft}
          </div>
          {hasAddon &&
            <div className="control input-addons">
              {children}
            </div>
          }
        </div>
        {hasText &&
          <p className="help" style={{color: '#a1a4a4', fontStyle: 'italic'}}>
            {Object.keys(messages).length > 0 && messages.text ? messages.text.number : 'This field contains only numbers'}
          </p>
        }
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
}

InputNumberFormat.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
  thousandSeparator: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  decimalSeparator: PropTypes.string,
  decimalScale: PropTypes.number,
  fixedDecimalScale: PropTypes.bool,
  allowNegative: PropTypes.bool,
  onChange: PropTypes.func,
  isAllowed: PropTypes.func,
  hasSymbol: PropTypes.bool,
  currency: PropTypes.string,
  width: PropTypes.number,
  disabled: PropTypes.bool,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'large',
    'normal'
  ]),
  displayType: PropTypes.oneOf(['input', 'text']),
  placeholder: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  hasText: PropTypes.bool,
  language: PropTypes.object,
  hasAddon: PropTypes.bool,
  loading: PropTypes.bool,
  hasIconRight: PropTypes.element,
  hasIconLeft: PropTypes.element,
  prefix: PropTypes.string,
  suffix: PropTypes.string,
  textAlign: PropTypes.string,
  styleInput: PropTypes.object,
};

InputNumberFormat.defaultProps = {
  disabled: false,
  size: 'small',
  displayType: 'input',
  fixedDecimalScale: false,
  hasText: true,
  language: {},
  thousandSeparator: true,
  decimalSeparator: '.',
  styleInput: {},
  allowNegative: false
};

export default InputNumberFormat;
