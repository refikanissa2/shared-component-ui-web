import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Input extends Component {

  constructor(props){
    super(props);
    this.state={
      errorInput: false,
      valueError: '',
      format: /~/,
      messages: this.props.language
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(e){
    if(e.target.value.match(this.state.format)){
      this.setState({
        errorInput: true,
        valueError: e.target.value.slice(-1)
      });
      return;
    }
    const value = e.target;
    this.setState({
      errorInput: false,
      valueError: ''
    });
    this.props.onChange(value);
  }

  validatePropsValue(){
    const {value} = this.props;
    const checkValue = value.match(this.state.format);

    if(checkValue) return false;

    return true;
  }

  render() {
    const {
      value,
      size,
      name,
      loading,
      type,
      disabled,
      hasAddon,
      children,
      placeholder,
      maxLength,
      width,
      index,
      error,
      styleInput,
      autocomplete,
      hasIconLeft,
      hasIconRight
    } = this.props;
    const {messages} = this.state;
    const validateValue = this.validatePropsValue(value) ? value : '';
    let className = 'input';
    let controlClassName = 'control';
    if(loading) controlClassName+=' is-loading';
    if(hasIconRight) controlClassName+= ' has-icons-right';
    if(hasIconLeft) controlClassName+=' has-icons-left';
    let typeInput = type ? type : 'text';
    let hasAddonClass = hasAddon ? 'has-addons' : '';
    let style = {};
    if (width) style = {width: width};
    let styles = styleInput;
    if(width) styles = {width: width, ...styles};
    if (size) className+=` is-${size}`;
    if (error) className+=' is-danger';
    if (this.state.errorInput) className+=' is-danger';
    return (
      <div style={style}>
        <div className={`field ${hasAddonClass}`}>
          <div className={controlClassName}>
            <input
              value={validateValue}
              name={name}
              className={className}
              type={typeInput}
              maxLength={maxLength}
              placeholder={placeholder}
              disabled={disabled}
              onChange={this.onChange}
              id={index}
              style={styles}
              pattern="[^~]+"
              title="Cannot Input '~'"
              autoComplete={autocomplete}
            />
            {hasIconRight && hasIconRight}
            {hasIconLeft && hasIconLeft}
          </div>
          {hasAddon &&
            <div className="control input-addons">
              {children}
            </div>
          }
        </div>
        {error && <p className="help is-danger">{error}</p>}
        {!error && this.state.errorInput === true && <p className="help is-danger">{messages.text.errorSpecialChar +' '+ this.state.valueError}</p>}
      </div>
    );
  }
}

Input.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  error: PropTypes.string,
  type: PropTypes.string,
  maxLength : PropTypes.number,
  onChange: PropTypes.func.isRequired,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'normal',
    'large'
  ]),
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  hasAddon: PropTypes.bool,
  width: PropTypes.number,
  styleInput: PropTypes.object,
  language: PropTypes.object.isRequired,
  autocomplete: PropTypes.string,
  hasIconLeft: PropTypes.element,
  hasIconRight: PropTypes.element
};

Input.defaultProps = {
  value: '',
  size: 'small',
  maxLength: 50,
  onChange() {},
  styleInput: {},
  autocomplete: 'off'
};

export default Input;
