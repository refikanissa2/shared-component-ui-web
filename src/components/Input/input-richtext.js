import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RichTextEditor from 'react-rte';

class InputRichText extends Component {

  constructor(){
    super();
    this.onChange = this.onChange.bind(this);
  }

  onChange(val){
    this.props.onChange(val.toString('html'), val);
  }

  render() {
    const {
      error, readOnly, value
    } = this.props;
    const toolbarConfig = {
      display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'LINK_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'HISTORY_BUTTONS'],
      INLINE_STYLE_BUTTONS: [
        {label: 'Bold', style: 'BOLD', className: 'custom-css-class'},
        {label: 'Italic', style: 'ITALIC'},
        {label: 'Underline', style: 'UNDERLINE'}
      ],
      BLOCK_TYPE_DROPDOWN: [
        {label: 'Normal', style: 'unstyled'},
        {label: 'Heading Large', style: 'header-one'},
        {label: 'Heading Medium', style: 'header-two'},
        {label: 'Heading Small', style: 'header-three'}
      ],
      BLOCK_TYPE_BUTTONS: [
        {label: 'Bullets', style: 'unordered-list-item'},
        {label: 'Numbering', style: 'ordered-list-item'}
      ]
    };
    return (
      <div className="field">
        <div className="control">
          <RichTextEditor
            value={value}
            onChange={this.onChange}
            className="new-post-editor"
            toolbarConfig={toolbarConfig}
            // toolbarClassName="demo-toolbar"
            // editorClassName="demo-editor"
            readOnly={readOnly}
          />
        </div>
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
}

InputRichText.propTypes = {
  error: PropTypes.string,
  onChange: PropTypes.func,
  readOnly: PropTypes.bool
};

InputRichText.defaultProps = {
  readOnly: false
};

export default InputRichText;
