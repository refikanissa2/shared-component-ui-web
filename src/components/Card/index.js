import Card from './card';
import CardHeader from './card-header';
import CardImage from './card-image';
import CardContent from './card-content';
import CardFooter from './card-footer';

Card.Header = CardHeader;
Card.Image = CardImage;
Card.Content = CardContent;
Card.Footer = CardFooter;

export default Card;
