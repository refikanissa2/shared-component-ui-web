import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = {
  value: {
    padding: 14,
    fontSize: 13,
  }
};

class ColumnValue extends Component {
  render() {
    const {padding, customStyle} = this.props;
    let valueStyle = styles.value;
    if(padding) valueStyle['padding'] = padding;
    if(customStyle) valueStyle = {...valueStyle, ...customStyle};
    return (
      <td style={valueStyle}>{this.props.children}</td>
    );
  }
}

ColumnValue.propTypes = {
  padding: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  customStyle: PropTypes.object,
};

ColumnValue.defaultProps = {
  padding: 14,
  customStyle: {}
};

export default ColumnValue;
