import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Column extends Component {
  render() {
    const {width} = this.props;
    const widthStyle = width ? `${width}%`:  '100%';

    return (
      <table width={widthStyle}>
        <tbody>
          {this.props.children}
        </tbody>
      </table>
    );
  }
}

Column.propTypes = {
  width: PropTypes.number
};

Column.defaultProps = {
  width: 100
};

export default Column;
