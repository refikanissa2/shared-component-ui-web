import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Option extends Component {
  constructor(){
    super();
    this.onDragStart = this.onDragStart.bind(this);
  }

  onDragStart(e){
    e.dataTransfer.setData('Text', e.target.index);
  }

  render() {
    const {value, disabled, children, draggable, index, padding} = this.props;
    let style = {};
    if(padding) style = {padding: padding};
    return (
      <option value={value}
        disabled={disabled}
        draggable={draggable}
        onDragStart={this.onDragStart}
        id={index}
        style={style}
      >{children}</option>
    );
  }
}

Option.propTypes = {
  value: PropTypes.string,
  onDragStart: PropTypes.func,
  draggable: PropTypes.bool,
  index: PropTypes.string,
  padding: PropTypes.number
};

Option.defaultProps = {
  value:'',
  onDragStart(){},
  draggable: false
};

export default Option;
