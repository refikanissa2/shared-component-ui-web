import React, { Component } from 'react';
import PropTypes from 'prop-types';

const color = {
  blue: '#74CEE2',
  grey: 'hsl(0, 0%, 86%)'
};

class SelectAutocomplete extends Component {

  constructor(){
    super();
    this.state = {
      openSuggestion: false,
      iconColor: color.grey
    };
    this.onChange = this.onChange.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  onChange(e){
    const value = e.target;
    this.props.onChange(value);
  }

  handleClick(e){
    if (this.node.contains(e.target)) {
      return;
    }
    this.setState({openSuggestion: false, iconColor: color.grey});
  }

  onKeyDown(){
  }

  componentWillMount(){
    document.addEventListener('click', this.handleClick, false);
  }

  componentWillUnmount(){
    document.removeEventListener('click', this.handleClick, false);
  }

  render() {
    const {value, name, size, loading, disabled, placeholder, hasAddon, suggestions, maxLength, width, children, error } = this.props;
    let {openSuggestion, iconColor} = this.state;
    let className = 'input';
    let loadingInput = loading ? 'is-loading' : '';
    let openSuggestionClass = openSuggestion ? 'autocomplete-open' : '';
    let iconSuggestion = openSuggestion ? 'fa-angle-up' : 'fa-angle-down';
    let hasAddonClass = hasAddon ? 'has-addons' : '';
    let style = width ? {width: width} : {};
    let suggestionsLength = suggestions.length;
    let styleIcon = {cursor: 'pointer', pointerEvents: 'auto', color: iconColor};
    if (error) className+=' is-danger';

    if (size) className+=` is-${size}`;
    return (
      <div className={`field ${hasAddonClass}`}>
        <div className={`control has-icons-right ${openSuggestionClass} ${loadingInput}`} style= {style}>
          <div  ref={node => { this.node = node; }}>
            <input
              value={value}
              name={name}
              className={className}
              type="text"
              maxLength={maxLength}
              placeholder={placeholder}
              disabled={disabled}
              onFocus={() => this.setState({openSuggestion: true, iconColor: color.blue})}
              onChange={this.onChange}
              style={style}
              onKeyDown={this.onKeyDown}
            />
            <span
              className={`icon is-${size} is-right`}
              style={styleIcon}
              onClick={() => this.setState({openSuggestion: !openSuggestion, iconColor: !openSuggestion == true ? color.blue : color.grey})}
              onMouseOver={() => {if(openSuggestion == false) this.setState({iconColor: color.blue});}}
              onMouseOut={() => {if(openSuggestion == false) this.setState({iconColor: color.grey});}}
            >
              <i className={`fa ${iconSuggestion}`}></i>
            </span>
            <div className="autocomplete-content" style= {style}>
              <div className="autocomplete-list">
                {suggestionsLength > 0 ? (
                  suggestions.map((suggestion, key) =>
                    <span
                      key={key}
                      className="autocomplete-item"
                      onClick={() =>{
                        this.props.onSelect(suggestion, name);
                        this.setState({openSuggestion: false, iconColor: color.grey});
                      }}
                    >{suggestion}</span>
                  )
                ) : (
                  <span className="autocomplete-item-empty">Data tidak ditemukan</span>
                )}
              </div>
            </div>
          </div>
        </div>
        {hasAddon &&
          <div className="control input-addons">
            {children}
          </div>
        }&nbsp;
        {error && <p className="help is-danger" style={{minWidth: 115}}>{error}</p>}
      </div>
    );
  }
}

SelectAutocomplete.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  name: PropTypes.string,
  placeholder: PropTypes.string,
  maxLength : PropTypes.number,
  onChange: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'large',
    'normal'
  ]),
  suggestions: PropTypes.array,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  hasAddon: PropTypes.bool,
  width: PropTypes.number
};

SelectAutocomplete.defaultProps = {
  value: '',
  suggestions: [],
  size: 'small',
  maxLength: 50,
  onChange() {},
  onSelect() {},
};

export default SelectAutocomplete;
