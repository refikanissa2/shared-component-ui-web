import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Select extends Component {

  constructor(props){
    super(props);
    this.state = {
      messages: this.props.language
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(e){
    const value = e.target;
    this.props.onChange(value);
  }

  render() {
    const {
      size,
      id,
      name,
      disabled,
      children,
      width,
      value,
      error,
      fullwidth,
      isLoadDropList,
      optionDisabled,
      changeMessageChoose
    } = this.props;
    const {messages} = this.state;
    let style = width ? {width: width,color:'#4a4a4a'} : {color:'#4a4a4a'};
    const sizeStyle = size ? `is-${size}` : '';
    const fullwidthStyle = fullwidth ? 'is-fullwidth' : '';
    const errorStyle = error ? 'is-danger' : '';
    return (
      <div className="field">
        <div className="control" style={{display: 'flex'}}>
          <div className={`select ${fullwidthStyle} ${sizeStyle} ${errorStyle}`}>
            <select
              id={id}
              name={name}
              disabled={disabled}
              style={style}
              onChange={this.onChange}
              value={value}
            >
              {changeMessageChoose ? (
                <option disabled={optionDisabled} value="">{changeMessageChoose}</option>
              ):(
                <option disabled={optionDisabled} value="">{messages.text.choose}</option>
              )}
              {children}
            </select>
          </div>&nbsp;
          {isLoadDropList && <img src={require('../../assets/img/loading.svg')} style={{width: '25px'}} />}
        </div>
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
}

Select.propTypes = {
  value: PropTypes.string,
  name: PropTypes.string,
  id: PropTypes.any,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'large',
    'normal'
  ]),
  disabled: PropTypes.bool,
  fullwidth: PropTypes.bool,
  width: PropTypes.number,
  isLoadDropList: PropTypes.bool,
  optionDisabled: PropTypes.bool,
  changeMessageChoose: PropTypes.string,
  language: PropTypes.object.isRequired
};

Select.defaultProps = {
  value: '',
  fullwidth: false,
  size: 'small',
  onChange() {},
  isLoadDropList: false,
  optionDisabled: true
};

export default Select;
