import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Options extends Component {
  render() {
    const {value, disabled, children, width, isWrapText} = this.props;
    let styles = {};
    if(width) styles = {width: width, maxWidth: width};
    if(isWrapText) styles = {...styles, wordWrap: 'break-word'};
    return <option value={value} disabled={disabled} style={styles}>{children}</option>;
  }
}

Options.propTypes = {
  // value: PropTypes.string
  width: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  isWrapText: PropTypes.bool
};

Option.defaultProps = {
  value:'',
  isWrapText: false
};

export default Options;
