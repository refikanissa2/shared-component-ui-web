import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Navbar extends Component {

  constructor(){
    super();
    this.state = {
      activeMobileMenu: false
    };
    this.toogleMenuMobile = this.toogleMenuMobile.bind(this);
  }

  toogleMenuMobile(){
    const {activeMobileMenu} = this.state;
    this.setState({activeMobileMenu: !activeMobileMenu});
  }

  render() {
    const {logo, title, children, container, styleNavbar, customStyleLogo} = this.props;
    let classContainer = 'container';
    if(container == 'fullwidth') classContainer += ' is-fullhd';
    if(container == 'fluid') classContainer += ' is-fluid';
    const {activeMobileMenu} = this.state;
    const activeClassMobile = activeMobileMenu ? 'is-active' : '';
    let styleLogo = {maxHeight: 38};
    if(customStyleLogo) styleLogo = {...styleLogo, ...customStyleLogo};
    return (
      <nav className="navbar is-transparent" style={styleNavbar}>
        <div className={classContainer}>
          <div className="navbar-brand">
            <a style={{display: 'flex', alignItems: 'center'}} href="/">
              {logo && <img src={logo} alt="Colekbayar" style={styleLogo}/>}
              {title && title}
            </a>
            <div className={`navbar-burger burger ${activeClassMobile}`} onClick={this.toogleMenuMobile}>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div className={`navbar-menu ${activeClassMobile}`}>
            {children}
          </div>
        </div>
      </nav>
    );
  }
}

Navbar.propTypes = {
  logo: PropTypes.string,
  title: PropTypes.string,
  container: PropTypes.oneOf([
    'fluid',
    'fullwidth'
  ]),
  styleNavbar: PropTypes.object,
  customStyleLogo: PropTypes.object
};

Navbar.defaultProps = {
  container: 'fullwidth',
  styleNavbar: {}
};

export default Navbar;
