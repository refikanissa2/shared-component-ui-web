import Navbar from './navbar';
import NavbarStart from './navbar-start';
import NavbarEnd from './navbar-end';
import NavbarItem from './navbar-item';
import NavbarDropdown from './navbar-dropdown';
import NavbarAccount from './navbar-account';

Navbar.Start = NavbarStart;
Navbar.End = NavbarEnd;
Navbar.Item = NavbarItem;
Navbar.Dropdown = NavbarDropdown;
Navbar.Account = NavbarAccount;
export default Navbar;
