import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';

class NavbarItem extends Component {
  render() {
    const {href, children, isBorder} = this.props;
    let style = {};
    if(isBorder === true) style = {...style, borderBottom: '1px solid'};
    return (
      <Link className="navbar-item " to={href} style={{fontSize: 15}}>
        <span style={style}>{children}</span>
      </Link>
    );
  }
}

NavbarItem.propTypes = {
  href: PropTypes.string,
  isBorder: PropTypes.bool
};

NavbarItem.defaultProps = {
  href: '#',
  isBorder: false
};

export default NavbarItem;
