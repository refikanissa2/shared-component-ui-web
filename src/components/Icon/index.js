import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Icon extends Component {
  render () {
    const {color, size, iconName, style, position, onClick, disabled} = this.props;
    let classSpan = 'icon';
    switch(color){
    case 'blue':
      classSpan += ' has-text-info';
      break;
    case 'dark':
      classSpan += ' has-text-dark';
      break;
    case 'green':
      classSpan += ' has-text-success';
      break;
    case 'orange':
      classSpan += ' has-text-warning';
      break;
    case 'red':
      classSpan += ' has-text-danger';
      break;
    default: classSpan += ' has-text-dark';
    }
    let classIcon = `fa ${iconName} ${size ? size: ''}`;
    if(position){
      if(position == 'left') classSpan+= ' is-left';
      else if(position == 'right') classSpan+= ' is-right';
    }
    let iconStyle = style;
    if(disabled){
      iconStyle = {...iconStyle, color: 'gray', cursor: 'no-drop'};
    }
    return (
      <span className={classSpan}>
        <i className={classIcon} style={iconStyle} onClick={disabled ? () => {} : onClick}></i>
      </span>
    );
  }
}

Icon.propTypes = {
  color: PropTypes.oneOf([
    'blue', 'dark', 'green', 'orange', 'red'
  ]),
  size: PropTypes.string,
  iconName: PropTypes.string.isRequired,
  style: PropTypes.object,
  position: PropTypes.oneOf([
    'left',
    'right'
  ]),
  onClick: PropTypes.func,
};

Icon.defaultProps = {
  color: 'dark',
  style: {},
  onClick(){}
};

export default Icon;
