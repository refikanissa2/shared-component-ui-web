import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dots, Levels, Sentry, Spinner, Squares, Digital, Bounce, Windmill } from 'react-activity';
import 'react-activity/dist/react-activity.css';

class LoadIndicator extends Component {
  render () {
    const {color, size, speed, animating, type} = this.props;
    switch(type){
    case 'dots':
      return (
        <Dots color={color} size={size} speed={speed} animating={animating} />
      );
    case 'levels':
      return (
        <Levels color={color} size={size} speed={speed} animating={animating} />
      );
    case 'sentry':
      return (
        <Sentry color={color} size={size} speed={speed} animating={animating} />
      );
    case 'spinner':
      return (
        <Spinner color={color} size={size} speed={speed} animating={animating} />
      );
    case 'squares':
      return (
        <Squares color={color} size={size} speed={speed} animating={animating} />
      );
    case 'digital':
      return (
        <Digital color={color} size={size} speed={speed} animating={animating} />
      );
    case 'bounce':
      return (
        <Bounce color={color} size={size} speed={speed} animating={animating} />
      );
    case 'windmill':
      return (
        <Windmill color={color} size={size} speed={speed} animating={animating} />
      );
    }
  }
}

LoadIndicator.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  speed: PropTypes.number,
  animating: PropTypes.bool,
  type: PropTypes.oneOf([
    'dots',
    'levels',
    'sentry',
    'spinner',
    'squares',
    'digital',
    'bounce',
    'windmill'
  ])
};

LoadIndicator.defaultProps = {
  color: '#BDBDBD',
  size: 17,
  speed: 1,
  animating: true,
  type: 'dots'
};

export default LoadIndicator;
