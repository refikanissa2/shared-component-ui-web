import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Pagination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: this.props.language
    };
  }
  

  createControls(){
    const {totalPages, number, setPages, totalElements} = this.props;
    let controls = [];

    if(totalPages < 11){
      for (let i = 0; i < totalPages; i++) {
        const activeClassName = i === number ? 'is-current' : '';
        controls.push(
          <li key={i}>
            <a
              className={`pagination-link ${activeClassName}`}
              onClick={() => setPages(i)}>{i+1}</a>
          </li>
        );
      }
    } else {
      if(number < 7){
        for (let i = 0; i < 12; i++) {
          const activeClassName = i === number ? 'is-current' : '';
          if(i == 10){
            controls.push(
              <li key={i}><span className="pagination-ellipsis">&hellip;</span></li>
            );
          } else if(i == 11){
            controls.push(
              <li key={i}>
                <a
                  className={`pagination-link ${activeClassName}`}
                  onClick={() => setPages(totalPages-1)}>{totalPages}</a>
              </li>
            );
          } else {
            controls.push(
              <li key={i}>
                <a
                  className={`pagination-link ${activeClassName}`}
                  onClick={() => setPages(i)}>{i+1}</a>
              </li>
            );
          }
        }
      } else if(number > (totalPages-1)-7){
        for (let i = 0; i < 12; i++) {
          const pageNumber = (totalPages-1)-10;
          const activeClassName = (pageNumber+i)-1 === number ? 'is-current' : '';
          if(i == 1){
            controls.push(
              <li key={i}><span className="pagination-ellipsis">&hellip;</span></li>
            );
          } else if(i == 0){
            controls.push(
              <li key={i}>
                <a
                  className="pagination-link"
                  onClick={() => setPages(0)}>{1}</a>
              </li>
            );
          } else {
            controls.push(
              <li key={i}>
                <a
                  className={`pagination-link ${activeClassName}`}
                  onClick={() => setPages((pageNumber+i)-1)}>{pageNumber+i}</a>
              </li>
            );
          }
        }
      } else {
        controls.push(
          <li key={0}>
            <a
              className="pagination-link"
              onClick={() => setPages(0)}>1</a>
          </li>
        );
        controls.push(
          <li key={1}><span className="pagination-ellipsis">&hellip;</span></li>
        );
        for (let i = 5; i > 1; i--) {
          const pageNumber = (number+1)-(i-1);
          controls.push(
            <li key={i}>
              <a
                className='pagination-link'
                onClick={() => setPages(pageNumber-1)}>{pageNumber}</a>
            </li>
          );
        }
        controls.push(
          <li key={6}>
            <a
              className="pagination-link is-current"
              onClick={() => setPages(number)}>{number+1}</a>
          </li>
        );
        for (let i = number; i < (number+4); i++) {
          const pageNumber = (number+2)+(i-number);
          controls.push(
            <li key={i}>
              <a
                className='pagination-link'
                onClick={() => setPages(pageNumber-1)}>{pageNumber}</a>
            </li>
          );
        }
        controls.push(
          <li key={totalElements-1}><span className="pagination-ellipsis">&hellip;</span></li>
        );
        controls.push(
          <li key={totalElements}>
            <a
              className="pagination-link"
              onClick={() => setPages(totalPages-1)}>{totalPages}</a>
          </li>
        );
      }
    }

    return controls;
  }

  render() {
    const {first, last, number, setPages} = this.props;
    const {messages} = this.state;
    return (
      <nav className="pagination is-small" role="navigation" aria-label="pagination" style={{display: 'inline-flex'}}>
        <ul className="pagination-list" style={{minWidth: 58, minHeight: 44}}>
          {!first &&
            <li><a
              className="pagination-link"
              onClick = {() => setPages(number - 1)}
            >{messages.button.before}</a></li>
          }
        </ul>
        <ul className="pagination-list">
          {this.createControls()}
        </ul>
        <ul className="pagination-list">
          {!last &&
            <li><a
              className="pagination-link"
              onClick = {() => setPages(number + 1)}
            >{messages.button.next}</a></li>
          }
        </ul>
      </nav>
    );
  }
}

Pagination.propTypes = {
  totalPages: PropTypes.number,
  language: PropTypes.object.isRequired
};

export default Pagination;
