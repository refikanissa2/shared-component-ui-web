import React, { Component } from 'react';
import Animate from 'rc-animate';
import './alert.css';
import Notice from './notice';

let seed = 0;

class Alert extends Component {

  constructor(){
    super();
    this.state = {
      alerts: []
    };
  }

  onEnd (key) {
    const alerts = this.state.alerts;
    const ret = [];
    let target;
    alerts.forEach((a) => {
      if (a.key === key) {
        target = a;
      } else {
        ret.push(a);
      }
    });
    if (target) {
      this.setState({
        alerts: ret,
      }, () => {
        if (target.callback) {
          target.callback();
        }
      });
    }
  }

  addAlert (a) {
    this.setState({
      alerts: this.state.alerts.concat(a),
    });
  }

  render() {
    const alerts = this.state.alerts;
    const self = this;
    const children = alerts.map((a) => {
      if (!a.key) {
        seed++;
        a.key = String(seed);
      }
      return <Notice {...a} onEnd={self.onEnd.bind(self, a.key)}/>;
    });
    return (
      <Animate transitionName="fade">{children}</Animate>
    );
  }
}

export default Alert;
