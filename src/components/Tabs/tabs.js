import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Tabs extends Component {
  constructor(){
    super();
    this.onClick = this.onClick.bind(this);
  }

  onClick(e){
    var menu = e.target.parentElement.dataset.name;
    this.props.onChangeMenu(menu);
  }

  render() {
    const {alignment, size, styles, fullwidth} = this.props;
    let className = 'tabs';

    if(alignment){
      switch(alignment){
      case 'center':
        className += ' is-centered';
        break;
      case 'right':
        className += ' is-right';
        break;
      }
    }

    if(size){
      switch(size){
      case 'small':
        className += ' is-small';
        break;
      case 'medium':
        className += ' is-medium';
        break;
      case 'large':
        className += ' is-large';
        break;
      }
    }

    if(styles){
      switch(styles){
      case 'border':
        className += ' is-boxed';
        break;
      case 'toggle':
        className += ' is-toggle';
        break;
      case 'round-toggle':
        className += ' is-toggle is-toggle-rounded';
        break;
      }
    }

    if(fullwidth){
      className += ' is-fullwidth';
    }else{
      className += '';
    }

    return (
      <div className={className}>
        <ul onClick={this.onClick}>
          {this.props.children}
        </ul>
      </div>
    );
  }
}

Tabs.propTypes = {
  alignment: PropTypes.oneOf([
    'center',
    'right'
  ]),
  size: PropTypes.oneOf([
    'small',
    'medium',
    'large'
  ]),
  styles: PropTypes.oneOf([
    'border',
    'toggle',
    'round-toggle'
  ]),
  onChangeMenu: PropTypes.func,
  fullwidth: PropTypes.bool
};

Tabs.defaultProps = {
  size: 'small',
  fullwidth: false,
  onChangeMenu(){}
};

export default Tabs;
