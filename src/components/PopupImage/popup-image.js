import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modal';

class componentName extends Component {
  render() {
    const { active, src, width, height, onClose } = this.props;
    return (
      <Modal active={active} onClose={() => onClose()}>
        <img src={src} width={width} height={height} />
      </Modal>
    );
  }
}

componentName.propTypes = {
  active: PropTypes.bool,
  src: PropTypes.string.isRequired,
  onClose: PropTypes.func,
  loading: PropTypes.bool
};

componentName.defaultProps = {
  loading: false
};

export default componentName;
