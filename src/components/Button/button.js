import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Button extends Component {
  render() {
    let className = 'button';
    const {size, fontColor, outlined, inverted, loading, disabled, type, block, role, children, index,display, customStyle} = this.props;
    let styles = {minWidth: 75};
    if(display) styles = {...styles, display: display};
    if (size) className+=` is-${size}`;
    if (outlined) {
      className+=' is-outlined';
    }else{
      styles = {...styles, color: fontColor};
    }
    if(customStyle) styles = {...styles, ...customStyle};
    if (inverted) className+=' is-inverted';
    if (loading) className+=' is-loading';
    if (block) className+=' is-fullwidth';
    if (type) className+=` is-${type}`;

    return <button
      className={className}
      type={role}
      disabled={disabled}
      onClick={this.props.onClick}
      style={styles}
      id={index}
    >{children}</button>;
  }
}

Button.propTypes = {
  outlined: PropTypes.bool,
  inverted: PropTypes.bool,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  block: PropTypes.bool,
  display: PropTypes.string,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'normal',
    'large'
  ]),
  role: PropTypes.oneOf([
    'button',
    'submit',
    'reset'
  ]),
  type: PropTypes.oneOf([
    'white',
    'light',
    'black',
    'dark',
    'link',
    'primary',
    'info',
    'success',
    'warning',
    'danger'
  ]),
  onClick: PropTypes.func,
  index: PropTypes.string,
  fontColor: PropTypes.string,
  customStyle: PropTypes.object,
};

Button.defaultProps = {
  onClick() {},
  type: 'primary',
  role: 'button',
  fontColor: 'white'
};

export default Button;
