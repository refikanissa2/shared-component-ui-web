import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modal';
import Button from '../Button';

class componentName extends Component {
  render() {
    const {active, loading, message, onConfirm, onClose, colorBtnYes, headerColor} = this.props;
    return (
      <Modal active={active}  onClose={loading ? () => {} : () => onClose()}>
        <Modal.Header title="Confirmation" backgroundColor={headerColor} />
        <Modal.Body>
          <span>{message}</span>
        </Modal.Body>
        <Modal.Footer>
          <p>
            <Button
              size="small"
              type={colorBtnYes}
              loading={loading}
              onClick={() => onConfirm()}
            >YES</Button>
            <Button
              size="small"
              type="primary"
              disabled={loading}
              onClick={() => onClose()}
            >NO</Button>
          </p>
        </Modal.Footer>
      </Modal>
    );
  }
}

componentName.propTypes = {
  active: PropTypes.bool,
  message: PropTypes.string,
  onConfirm: PropTypes.func,
  onClose: PropTypes.func,
  colorBtnYes: PropTypes.oneOf([
    'primary',
    'warning',
    'danger',
    ''
  ]),
  loading: PropTypes.bool,
  headerColor: PropTypes.string
};

componentName.defaultProps = {
  colorBtnYes: 'danger',
  loading: false
};

export default componentName;
