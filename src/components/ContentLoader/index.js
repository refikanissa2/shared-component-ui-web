import ContentLoader from './content-loader';
import ContentLoaderDetail from './content-loader-detail';

ContentLoader.Detail = ContentLoaderDetail;

export default ContentLoader;
