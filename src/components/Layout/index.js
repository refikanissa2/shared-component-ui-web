import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = {
  container: {
    width: '100%',
    minHeight: '100%',
    position: 'relative',
    // display: 'contents'
  }
};

class Layout extends Component {
  render() {
    const {background, children, customStyle} = this.props;
    let style = styles.container;
    style = {...style, background: background};
    if(customStyle) style = {...style, ...customStyle};

    return <div style={style}>{children}</div>;
  }
}

Layout.propTypes = {
  background: PropTypes.string,
  customStyle: PropTypes.object,
};

Layout.defaultProps = {
  background: '#ffffff'
};

export default Layout;
