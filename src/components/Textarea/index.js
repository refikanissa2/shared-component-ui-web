import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Textarea extends Component {

  constructor(){
    super();
    this.onChange = this.onChange.bind(this);
  }

  onChange(e){
    const value = e.target;
    this.props.onChange(value);
  }

  render() {
    const {value, size, name, loading, disabled, placeholder, maxLength, width, error, rows, id } = this.props;
    let className = 'textarea';
    let loadingInput = loading ? 'is-loading' : '';
    let style = width ? {minWidth: '20%', width: width, resize: 'none'} : {resize: 'none'};
    if (size) className+=` is-${size}`;
    if (error) className+=' is-danger';
    return (
      <div className="field">
        <div className={`control ${loadingInput}`}>
          <textarea
            value={value}
            name={name}
            id={id}
            className={className}
            type="text"
            maxLength={maxLength}
            placeholder={placeholder}
            disabled={disabled}
            onChange={this.onChange}
            style={style}
            rows={rows}
          ></textarea>
        </div>
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
}

Textarea.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  rows: PropTypes.string,
  cols: PropTypes.string,
  maxLength : PropTypes.number,
  onChange: PropTypes.func.isRequired,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'normal',
    'large'
  ]),
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  width: PropTypes.number,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ])
};

Textarea.defaultProps = {
  value: '',
  rows: '',
  cols: '',
  size: 'small',
  maxLength: 255,
  onChange() {}
};

export default Textarea;
