import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';

class TextCurrency extends Component {
  render() {
    const {
      value,
      thousandSeparator, decimalSeparator, decimalScale,fixedDecimalScale,
      allowNegative,
      width,
      prefix, suffix
    } = this.props;
    let style = width ? {width: width} : {};
    return (
      <NumberFormat
        value={value}
        thousandSeparator={thousandSeparator}
        decimalSeparator={decimalSeparator}
        decimalScale={decimalScale}
        fixedDecimalScale={fixedDecimalScale}
        allowNegative={allowNegative}
        style={style}
        prefix={prefix}
        suffix={suffix}
        displayType="text"
      />
    );
  }
}

TextCurrency.propTypes = {
  value: PropTypes.string,
  thousandSeparator: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string,
  ]),
  decimalSeparator: PropTypes.string,
  decimalScale: PropTypes.number,
  fixedDecimalScale: PropTypes.bool,
  allowNegative: PropTypes.bool,
  width: PropTypes.number,
  prefix: PropTypes.string,
  suffix: PropTypes.string,
};

TextCurrency.defaultProps = {
  thousandSeparator: true,
  decimalSeparator: '.',
};

export default TextCurrency;
